Tomcat based on OpenJDK and Alpine
====================================

This image is based on [OpenJDK Alpine](https://hub.docker.com/_/openjdk/).

The **examples** and **docs** have been cleaned up in tomcat.

Usage Example
-------------
Run in an interactive mode and watch the log
```
$ docker run -it --rm splazit/tomcat
```
Run in a daemon mode, expose port 8080 to host and mount **webapps** and **logs** dir to the host
```
$ docker run -d -p 8080:8080 -v [host_path_webapps_dir]:/tomcat/webapps -v [host_path_logs_dir]:/tomcat/logs splazit/tomcat
```
Once you have run the above command, you should be able to connect to your http://localhost:8080/

Feel free to leave any suggestions.